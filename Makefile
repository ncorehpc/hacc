#------------------------------------------------------------------------------------
# Copyright (C) Arm Limited, 2019-2020 All rights reserved.
#
# The example code is provided to you as an aid to learning when working
# with Arm-based technology, including but not limited to programming tutorials.
#
# Arm hereby grants to you, subject to the terms and conditions of this Licence,
# a non-exclusive, non-transferable, non-sub-licensable, free-of-charge copyright
# licence, to use, copy and modify the Software solely for the purpose of internal
# demonstration and evaluation.
#
# You accept that the Software has not been tested by Arm therefore the Software
# is provided "as is", without warranty of any kind, express or implied. In no
# event shall the authors or copyright holders be liable for any claim, damages
# or other liability, whether in action or contract, tort or otherwise, arising
# from, out of or in connection with the Software or the use of Software.
#------------------------------------------------------------------------------------

#include config.mk

#ITERATIONS = 1000

#SRC = GravityForceKernel.cpp main.cpp
#
#EXE_NEON   = hacc_$(COMPILER)_neon.exe
#EXE_SVE    = hacc_$(COMPILER)_sve.exe
#
#EXE = $(EXE_NEON) $(EXE_SVE)
#
## Runtime parameters
#OMP_ENV = OMP_NUM_THREADS=48 OMP_PROC_BIND=close
#NUMACTL = numactl -C 0-47 -l
#RUN = $(OMP_ENV) $(NUMACTL)
#
#
#.PHONY: all clean run
#
#all: $(EXE)
#
#clean:
#	rm -f *.o *.exe *.opt.yaml
#
#run: $(EXE)
#	$(call print_hline)
#	$(RUN) ./$(EXE_NEON) $(ITERATIONS)
#	$(call print_hline)
#	$(RUN) ./$(EXE_SVE) $(ITERATIONS)
#	$(call print_hline)
#
#$(EXE_NEON): $(SRC)
#	$(call print_hline)
#	$(call print_version)
#	$(CXX) $(CXXFLAGS_REPORT) $(CXXFLAGS_OPT) $(CXXFLAGS_NEON) $(CXXFLAGS_OPENMP) -o $@ $^
#	$(call print_hline)
#
#$(EXE_SVE): $(SRC)
#	$(call print_hline)
#	$(call print_version)
#	$(CXX) $(CXXFLAGS_REPORT) $(CXXFLAGS_OPT) $(CXXFLAGS_OPENMP) -o $@ $^
#	$(call print_hline)

ifeq ($(CROSS_COMPILE), )
	CROSS_COMPILE :=$(RISCV_PATH)/bin/riscv64-unknown-linux-gnu-
endif

CXX = $(CROSS_COMPILE)clang++

OPT_FLAGS = -O2 -Wall -Wno-unused-but-set-variable -Wno-unused-variable -mabi=lp64d \
            -march=rv64imafdcvh_zic64b_zicbop_zicboz_ziccamoa_ziccif_ziccrse_zicsr_zifencei_zihintntl_zihintpause_za64rs_zfh_zba_zbb_zbs_zkt_zvfh_sscofpmf_svinval_svnapot_xsfvfhbfmin_xsfvfnrclipxfqf_xsfvfwmaccqqq_xsfvqmaccqoq_zvl512b \
            -menable-experimental-extensions -mtune=sifive-7-series -mcpu=sifive-x280o

RISCV_CFLAGS += -O2 -stdlib=libstdc++ -lm -menable-experimental-extensions  -mcmodel=medany -target riscv64-unknown-linux -ffunction-sections -fno-integrated-as -mtune=sifive-7-series -mcpu=sifive-x280 -mabi=lp64d -msifive-recode=neon -fopenmp
RISCV_CFLAGS += $(OPT_FLAGS)
RISCV_CFLAGS += $(OPT_FLAGS_VEC)

RISCV_CFLAGS += -fassociative-math -ffast-math -menable-unsafe-fp-math -mllvm -force-target-max-vector-interleave=1 

ifneq (,$(findstring 13.0,$(CROSS_COMPILE)))
	RISCV_CFLAGS += -mllvm -force-vector-width=64 
ifeq ($(OPT_FLAGS_VEC), )
	RISCV_CFLAGS += -mllvm --riscv-v-vector-bits-min=512
endif
else 
	RISCV_CFLAGS += -mllvm --vector-primary-lmul-max=2
endif 

all: compile build_vars

compile:
	$(CXX) -o HACCKernels $(RISCV_CFLAGS) $(CXXFLAGS) main.cpp GravityForceKernel.cpp 
clean:
	rm -f HACCKernels build_vars.txt
build_vars:
	@echo "COMPILER:$(CXX)" > build_vars.txt
	@echo "CFLAGS:$(RISCV_CFLAGS)" >> build_vars.txt
	@echo "CXXFLAGS:$(CXXFLAGS)" >> build_vars.txt